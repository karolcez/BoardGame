class FieldsEventsContainer {
    constructor(fieldsCount) {
        this.fieldsEvents = [];
    }
    inicializeFields(fieldsCount) {
        for (let index = 0; index < fieldsCount; index++) {
            this.fieldsEvents.push(null);
        }
    }
    addSpecialEvenToField(fieldNumber, specialEventObject) {
        this.fieldsEvents[fieldNumber - 1] = specialEventObject;
    }
}

class MovesContainer {
    constructor() {
        this.moves = [];
    }
    addMove(randomNumber) {
        this.moves.push(randomNumber);
    }
    getAverage() {
        if (this.moves.length == 0) {
            return 0;
        }
        else {
            let summary = 0;
            this.moves.forEach(move => {
                summary += move;
            });

            return Math.round(10 * summary / this.moves.length) / 10;
        }
    }
    getGameResult() {
        return "Number of moves: " + this.moves.length + "\nMoves average: " + this.getAverage();
    }
}

class SpecialEvent {
    constructor(event, eventParameters) {
        this.event = event;
        this.eventParameters = eventParameters;
    }
}
class FieldEvent
{
    static gameLose(parameters) {
        alert("You lose!");
        alert(parameters.movesContainer.getGameResult());
        return parameters.pawnPosition;
    }
    static gameWin(parameters) {
        alert("You win!");
        alert(parameters.movesContainer.getGameResult());
        return parameters.pawnPosition;
    }
    static moveBack(parameters) {
        alert("Move back " + this.eventParameters.fieldsToMoveBack + " fields");
        return parameters.pawnPosition - this.eventParameters.fieldsToMoveBack;
    }
}


class GameController {
    constructor() {
        this.fieldsCount = 20;
        this.pawnPosition = 0;
        this.fieldsEventsContainer = new FieldsEventsContainer();
        this.movesContainer = new MovesContainer();
    }
    checkSpecialFields() {

        if (this.fieldsEventsContainer.fieldsEvents[this.pawnPosition] != null) {
            let oldPawnPosition = this.pawnPosition;

            this.pawnPosition = this.fieldsEventsContainer
                .fieldsEvents[this.pawnPosition].event({ "pawnPosition": this.pawnPosition, "movesContainer": this.movesContainer });

            if (this.pawnPosition != oldPawnPosition) {
                this.checkSpecialFields();
            }
        }
    }
    updateBoardView() {
        for (let index = 0; index < this.fieldsCount; index++) {
            $("#field" + index).removeClass("pawn");
        }
        $("#field" + this.pawnPosition).addClass("pawn");
    }
    nextMove() {
        let rand = Math.floor(Math.random() * 6) + 1;
        alert("Dice result: " + rand);
        $("#dice-result > span").text(rand);

        this.movesContainer.addMove(rand);
        this.pawnPosition += rand;
        if (this.pawnPosition > this.fieldsCount - 1) {
            this.pawnPosition = this.fieldsCount - (this.pawnPosition - this.fieldsCount) - 2;
        }
        this.checkSpecialFields();
        this.updateBoardView();
    }

    createFields(specialFields) {
        this.fieldsEventsContainer.inicializeFields(this.fieldsCount);
        specialFields.forEach(specialField => {
            this.fieldsEventsContainer.addSpecialEvenToField(specialField.fieldNumber, specialField.eventObject);
        });
    }
}

var gameController = new GameController();
gameController.createFields([
    { "fieldNumber": 12, "eventObject": new SpecialEvent(FieldEvent.gameLose) },
    { "fieldNumber": 20, "eventObject": new SpecialEvent(FieldEvent.gameWin) },
    { "fieldNumber": 19, "eventObject": new SpecialEvent(FieldEvent.moveBack, { "fieldsToMoveBack": 8 }) }
]);


var handleClickRollwDiceButton = function () {
    gameController.nextMove();
}